!function ($) {
    "use strict"; // jshint ;_;
    var GridPagerInit = function (element, options) {
        this.init(element, options);
    };
    var c = function(r){console.log(r)};
    GridPagerInit.prototype = {
        constructor: GridPagerInit
        , init: function (element, options) {
            var GPI = this
                , $el = $(element)
                , controls
                , $cols
                , $numItems, i, ii
                , pgnow = 1
                , pgmin = 1
                , btn = []
                , elli = "...";
            GPI.$el = $el;
            GPI.$options  = GPI.getOptions(options);
            GPI.$controls = controls;
            GPI.$cols     = $el.find(GPI.$options.cls);//reference to all classname els found within container
            GPI.$numItems = GPI.$cols.length;//number of classname els found within container
            GPI.$pgmin    = pgmin;
            GPI.$pgnow    = pgnow;//TODO: Get $pgnow updated around the world
            GPI.$pgmax    = Math.ceil(GPI.$numItems/GPI.$options.tpp);
            GPI.$currNavs      = btn;
            GPI.$elli     = elli;
            GPI.setStyles();
            //GPI.showPage('#'+GPI.$options.id+' li a');
            if(GPI.shouldPaginate() == true){
                var firstNav = GPI.initNavs();
                GPI.showPage(firstNav);
                GPI.listen();
            }else{
                GPI.showPage();
            }
            //c(GPI.shouldPaginate());
        }
        , initNavs: function(){//_________________SETUP THE CONTROLS_________________
            var GPI = this;
            var controls_html = '<nav class="text-center" id="'+GPI.$options.id+'"><ul class="gpagination">'
                , i
                , ii = -1+GPI.$options.tpp
                , j, navul;
            for(j=0;j<GPI.$pgmax;j++){//______ADD <LI><A> ELEMENTS TO <UL> WITH DATA-GO-TO
                controls_html += '<li><a href="javascript:void(0);" data-go-to="'+j+'">'+(j+1)+'</a></li>';
            }
            controls_html += '</ul></nav>';
            GPI.$navnav = GPI.$el.after(controls_html);
            //GPI.$el.after(controls_html);
            //GPI.showPage('#'+GPI.$options.id+' li a');
            //TODO: not passing the element to showPage properly.
            //return first nav to pass to showPage
            navul = $('#'+GPI.$options.id+' ul.pagination');
            return GPI.$pgnow = $(":first-child>a", navul);
        }
        , updateNavs: function(newpg){//______CHANGE THE CONTROLS - CALLS SEL AND NUMS_________________
            var GPI = this;
            // save selected jQuery objects into variable
            GPI.$currNavs = $('#'+GPI.$options.id+'>ul li>a');
            //GPI.$navnav.empty();
                 if ( newpg == GPI.$pgmin+0                     ){ GPI.sel(1); GPI.nums_bot(); }
            else if ( newpg == GPI.$pgmin+1                     ){ GPI.sel(2); GPI.nums_bot(); }
            else if ( newpg == GPI.$pgmin+2                     ){ GPI.sel(3); GPI.nums_bot(); }
            else if ( newpg == GPI.$pgmin+3                     ){ GPI.sel(4); GPI.nums_bot(); }
            else if ( newpg == GPI.$pgmin+4                     ){ GPI.sel(5); GPI.nums_bot(); }
            else if ( newpg  > GPI.$pgmin+4&&newpg<GPI.$pgmax-4 ){ GPI.sel(5); GPI.nums_mid(); }
            else if ( newpg == GPI.$pgmax-4                     ){ GPI.sel(5); GPI.nums_top(); }
            else if ( newpg == GPI.$pgmax-3                     ){ GPI.sel(6); GPI.nums_top(); }
            else if ( newpg == GPI.$pgmax-2                     ){ GPI.sel(7); GPI.nums_top(); }
            else if ( newpg == GPI.$pgmax-1                     ){ GPI.sel(8); GPI.nums_top(); }
            else if ( newpg == GPI.$pgmax-0                     ){ GPI.sel(9); GPI.nums_top(); }
        }
        , sel: function(button){//______INDICATE CURRENT BUTTON_________________
            $(".pagbtn").css('backgroundColor', 'teal' );//reset all colors
            $("#btn"+button ).css('backgroundColor', 'blue' );//set
        }
        , nums_bot: function(){//_______REASSIGN BUTTONS TEXT AND LINKS (BOTTOM)_________________
            var GPI = this, i;
            GPI.$currNavs.eq(8).textContent = GPI.$elli;//b8 ellipsis, 1-7 low num range
            for(i=1;i<=7;i++){               //count up from min
                GPI.$currNavs.eq(i).textContent = i; //set text within 9btns
                GPI.$currNavs.eq(i).attr( 'data-go-to', i );
            }
        }
        , nums_mid: function(curpg){//_______REASSIGN BUTTONS TEXT AND LINKS (MID)_________________
            var GPI = this;
            GPI.$currNavs.eq(2).textContent = GPI.$elli ; GPI.$currNavs.eq(2).data('goTo', ''   );/*both ellipsis, dynamic nums*/
            GPI.$currNavs.eq(3).textContent = curpg-2   ; GPI.$currNavs.eq(3).data('goTo', curpg-2 );
            GPI.$currNavs.eq(4).textContent = curpg-1   ; GPI.$currNavs.eq(4).data('goTo', curpg-1 );
            GPI.$currNavs.eq(5).textContent = curpg     ; GPI.$currNavs.eq(5).data('goTo', curpg   );
            GPI.$currNavs.eq(6).textContent = curpg+1   ; GPI.$currNavs.eq(6).data('goTo', curpg+1 );
            GPI.$currNavs.eq(7).textContent = curpg+2   ; GPI.$currNavs.eq(7).data('goTo', curpg+2 );
            GPI.$currNavs.eq(8).textContent = GPI.$elli ; GPI.$currNavs.eq(8).data('goTo', ''   );
        }
        , nums_top: function(){//_______REASSIGN BUTTONS TEXT AND LINKS (TOP)_________________
            var GPI = this, i;
            GPI.$currNavs.eq(2).textContent = GPI.$elli;/*b2 ellipsis, 9-3 high num range*/
            for(i=GPI.$pgmax; i>=GPI.$pgmax-6; i--){    /*count down from max*/
                GPI.$currNavs.eq(i).textContent = i;  /*set text within #$currNavss*/
                GPI.$currNavs.eq(i).attr( 'data-go-to', i );
            }
        }

        //_______[CALLED BY: INIT/LISTEN]- TAKES PG NUM ARG [DATA-GO-TO VAL] TO SHOW/HIDE GRID ITEMS <<<<<<<<<<<<<<
        //_______AND CALL UPDATENAVS PASSING CHOSEN [DATA-GO-TO VAL],
        , showPage: function (e){
            var GPI = this
                , startshow          /* 7*tpp    =24 -> start*/
                , endshow           /* 24+tpp-1 =27 -> end*/
                , i, ii, j;
            GPI.$pgnow = $(e.currentTarget).data('go-to');       /*clicked control with datagoto7, pg=7*/
            if(GPI.$pgnow == undefined){
                GPI.$pgnow = 1;
            }
            startshow = parseInt(GPI.$pgnow*GPI.$options.tpp);   /* 7*tpp    =24 -> start*/
            endshow = parseInt((startshow+GPI.$options.tpp)-1);  /* 24+tpp-1 =27 -> end*/
/*
            for(i=GPI.$numItems;i>ii;i--){//______HIDE ALL ITEMS
                GPI.$el.find(GPI.$options.cls+':eq('+i+')').hide();
            }/!* i is 43, loop counting down until TPP-1 (3) is reached (42x)*!/
*/
            for(i=0;i<GPI.$numItems;i++){//[i] will count up to total as items with 'cls' setting (.grid-pager)
                if(i < startshow || i > endshow){//_______HIDE EVERYTHING OUTSIDE OF STARTSHOW
                    GPI.$el.find(GPI.$options.cls+':eq('+i+')').hide();
                }
                else {//_______SHOW the .grid-pager elmts with index of i
                    GPI.$el.find(GPI.$options.cls+':eq('+i+')').show();
                }
            }
            GPI.updateNavs(GPI.$pgnow);
        }
        , listen: function(){
            var GPI = this;
            //"pager" is $("grid-pager-nav li a") elmts by default (all a elmts in li elmts in container)
            var pager = $('#'+GPI.$options.id+' li a');
            //onClick call showPage with the 'a' elmt as the "e" argument
            pager.on('click',  $.proxy(GPI.showPage, GPI));
        }


        , getOptions: function (options) {
            var GPI = this;
            options = $.extend({}, $.fn['gridPager'].defaults, options, GPI.$el.data());
            return options;
        }
        , setStyles: function(){
            var css = "\
                .gpagination {\
                    display: inline-block;\
                    padding-left: 0;\
                    margin: 21px 0;\
                    border-radius: 4px;\
                }\
                .gpagination>li {\
                    display: inline;\
                }\
                .gpagination>li:first-child>a, \
                .gpagination>li:first-child>span {\
                    margin-left: 0;\
                    border-bottom-left-radius: 4px;\
                    border-top-left-radius: 4px;\
                }\
                .gpagination>li:last-child>a, \
                .gpagination>li:last-child>span {\
                    border-bottom-right-radius: 4px;\
                    border-top-right-radius: 4px;\
                }\
                .gpagination>li>a:hover, \
                .gpagination>li>span:hover, \
                .gpagination>li>a:focus, \
                .gpagination>li>span:focus {\
                    color: #ffffff;\
                    background-color: #00dba3;\
                    border-color: transparent;\
                }\
                .gpagination>li>a, \
                .gpagination>li>span {\
                    position: relative;\
                    float: left;\
                    padding: 10px 15px;\
                    line-height: 1.42857143;\
                    text-decoration: none;\
                    color: #ffffff;\
                    background-color: #00bc8c;\
                    border: 1px solid transparent;\
                    margin-left: -1px;\
                }";
            var tempDiv = document.createElement('div');tempDiv.innerHTML = '<p>p</p><style>' + css + '</style>';
            document.getElementsByTagName('head')[0].appendChild(tempDiv.childNodes[1]);
        }
        , shouldPaginate: function(){var GPI = this;if(GPI.$numItems > GPI.$options.tpp){return true;}else{return false;}}

    };//END:GridPagerInit.prototype

    /* GridPagerInit PLUGIN DEFINITION
     * ======================== */
    $.fn.gridPager = function (option) {
        return this.each(function () {
            var $this = $(this)
                , data = $this.data('gridPager')
                , options = typeof option == 'object' && option;
            if (!data) $this.data('gridPager', (data = new GridPagerInit(this, options)));
            if (typeof option == 'string') data[option]()
        })
    };

    $.fn.gridPager.Constructor = GridPagerInit;
    /* GridPagerInit DATA-API
     * =============== */
    $.fn.gridPager.defaults = {
        cls: '.grid-pager',
        tpp: 12
    }

}(window.jQuery);